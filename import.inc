<?php

function opensecrets_open_data_campaign_finance_import($cycle) {
  set_time_limit(0);

  $suffix = substr($cycle, 2);
  $tables = array(
    'cands' => array('cycle', 'fec_cand_id', 'cid', 'first_last_p', 'party', 'dist_id_run_for', 'dist_id_curr', 'curr_cand', 'cycle_cand', 'crp_ico', 'recip_code', 'no_pacs'),
    'cmtes' => array('cycle', 'cmte_id', 'pac_short', 'affiliate', 'ult_org', 'recip_id', 'recip_code', 'fec_cand_id', 'party', 'prim_code', 'source', 'sensitive_', 'foreign_', 'active'),
    'pacs' => array('cycle', 'fec_rec_no', 'pac_id', 'cid', 'amount', 'date', 'real_code', 'type', 'di'),
    'pac_other' => array('cycle', 'fec_rec_no', 'filer_id', 'donor_cmte', 'contrib_lend_trans', 'city', 'state', 'zip', 'fec_occ_emp', 'prim_code', 'date', 'amount', 'recip_id', 'party', 'other_id', 'recip_code', 'recip_primcode', 'amend', 'report', 'pg', 'microfilm', 'type', 'realcode', 'source'),
    'indivs' => array('cycle', 'fec_trans_id', 'contrib_id', 'contrib', 'recip_id', 'orgname', 'ult_org', 'real_code', 'date', 'amount', 'street', 'city', 'state', 'zip', 'recipcode', 'type', 'cmte_id', 'other_id', 'gender', 'fec_occ_emp', 'microfilm', 'occ_ef', 'emp_ef', 'source'),
  );
  foreach ($tables as $name => $columns) {
    $file_name = variable_get('opensecrets_open_data_data_directory', '') . '/' . $name . $suffix . '.csv';
    if (!is_readable($file_name)) {
      watchdog('opensecrets_open_data', 'Could not read file %file.', array('%file' => $file_name), WATCHDOG_ERROR);
      continue;
    }
    $file = fopen($file_name, 'rb');
    db_query("DELETE FROM {opensecrets_open_data_" . $name . "} WHERE cycle = '%s'", $cycle);
    $line_number = 0;
    $column_count = count($columns);

    while (TRUE) {
      $tmp = fopen('php://temp', 'r+b');
      // Filter 10000 lines and save in temporary memory.
      $count = 0;
      while ($count < 10000 && ($line = fgets($file)) !== FALSE) {
        $count += 1;
        // Remove trailing single backslashes and quotes.
        $line = preg_replace(array('/(?<!\\\)\\\",/', '/(?<!,)"",/'), array('",', '",'), $line);
        fwrite($tmp, $line);
      }

      // Done?
      if ($count === 0) {
        break;
      }

      rewind($tmp);
      while (($line = fgetcsv($tmp)) !== FALSE) {
        $line_number += 1;
        if (count($line) !== $column_count) {
          watchdog('opensecrets_open_data', 'Line @line_number of %file has the wrong number of columns.', array('%file' => $file_name, '@line_number' => $line_number), WATCHDOG_WARNING);
        }
        else {
          $line = array_combine($columns, $line);
          if (!empty($line['date']) && preg_match('!^([0-9]+)/([0-9]+)/([0-9]+)!', $line['date'], $matches)) {
            list(, $month, $day, $year) = $matches;
            if (strlen($year) === 2) {
              $year = '20'. $year;
            }
            $line['date_int'] = gmmktime(0, 0, 0, (int) $month, (int) $day, (int) $year);
          }
          drupal_write_record('opensecrets_open_data_' . $name, $line);
        }
      }
      fclose($tmp);
    }
    fclose($file);
  }
}
